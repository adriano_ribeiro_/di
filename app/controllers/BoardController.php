<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 14:12
 */

namespace app\controllers;


use app\services\board\BoardServiceInterface;
use Throwable;


class BoardController
{

    /**
     * @var BoardServiceInterface
     */
    private $service;


    /**
     * BoardView constructor.
     * @param BoardServiceInterface $service
     */
    public function __construct(BoardServiceInterface $service)
    {
        $this->service = $service;
    }

    public function store()
    {
        try {
            return $this->service->save($_POST);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }
}