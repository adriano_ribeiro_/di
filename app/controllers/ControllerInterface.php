<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 30/01/2019
 * Time: 09:23
 */

namespace app\controllers;


interface ControllerInterface
{
    public function store();
    public function update();
    public function updateStatus();
    public function all();
    public function find();
}