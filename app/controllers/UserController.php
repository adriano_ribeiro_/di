<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 09:20
 */

namespace app\controllers;



use app\services\user\UserServiceInterface;
use Throwable;

class UserController implements ControllerInterface
{

    /**
     * @var UserServiceInterface
     */
    private $service;


    /**
     * UserController constructor.
     * @param UserServiceInterface $service
     */
    public function __construct(UserServiceInterface $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        try {
            return $this->service->home();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function auth() {
        try {
            return $this->service->auth();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function logout()
    {
        try {
            return $this->service->logout();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function store() {
        try {
            return $this->service->save($_POST);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function updateStatus()
    {
        // TODO: Implement updateStatus() method.
    }

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function find()
    {
        // TODO: Implement find() method.
    }
}