<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 28/01/2019
 * Time: 11:10
 */

namespace app\factories;


use app\models\Board;


class BoardFactory implements FactoryInterface
{

    public static function make(array $data)
    {
        try {
            $board = new Board();
            return $board
                ->setName(trim($data['name']))
                ->setStatus(trim($data['status']))
            ;
        } catch (\Throwable $e) {
            $e->getMessage();
        }
    }
}