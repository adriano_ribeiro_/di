<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 30/01/2019
 * Time: 09:03
 */

namespace app\factories;


use app\models\User;
use app\utils\Password;

class UserFactory implements FactoryInterface
{

    public static function make(array $data)
    {
        $user = new User();
        return $user
            ->setName(trim($data['name']))
            ->setEmail(trim($data['email']))
            ->setPassword(Password::hash(trim($data['password'])))
            ->setFunction($data['function_id'])
            ->setStatus((bool)$data['status'])
        ;
    }
}