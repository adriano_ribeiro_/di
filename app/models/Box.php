<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/11/2018
 * Time: 07:52
 */

namespace app\models;

use app\traits\FieldsGeneric;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repositories\box\BoxRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="boxes")
 */
class Box extends Model
{
    use FieldsGeneric;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="app\models\User", inversedBy="box")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo garçom obrigatório")
     */
    private $user;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="app\models\Product", inversedBy="boxes")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo produto obrigatório")
     */
    private $product;

    /**
     * @var float $priceUnit
     *
     * @ORM\Column(type="decimal", name="price_unit", precision=10, scale=2, nullable=false)
     *
     * @Assert\NotBlank(message="Campo preço unitário obrigatório")
     */
    private $priceUnit;

    /**
     * @var float $valueAll
     *
     * @ORM\Column(type="decimal", name="value_all", precision=10, scale=2, nullable=false)
     *
     * @Assert\NotBlank(message="Campo valor total obrigatório")
     */
    private $valueAll;

    /**
     * @var boolean $status
     *
     * @ORM\Column(type="boolean", length=1, nullable=false)
     *
     * @Assert\NotBlank
     */
    private $status;

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Box
     */
    public function setUser(User $user): Box
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return Box
     */
    public function setProduct(Product $product): Box
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceUnit(): float
    {
        return $this->priceUnit;
    }

    /**
     * @param float $priceUnit
     * @return Box
     */
    public function setPriceUnit(float $priceUnit): Box
    {
        $this->priceUnit = $priceUnit;
        return $this;
    }

    /**
     * @return float
     */
    public function getValueAll(): float
    {
        return $this->valueAll;
    }

    /**
     * @param float $valueAll
     * @return Box
     */
    public function setValueAll(float $valueAll): Box
    {
        $this->valueAll = $valueAll;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return Box
     */
    public function setStatus(bool $status): Box
    {
        $this->status = $status;
        return $this;
    }
}