<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/11/2018
 * Time: 07:54
 */

namespace app\models;


use app\traits\FieldsGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repositories\user\FunctionUserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="functions")
 */
class FunctionUser extends Model
{
    use FieldsGeneric;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", unique=true, length=100, nullable=false)
     *
     * @Assert\NotBlank(message="Campo nome obrigatório")
     */
    private $name;

    /**
     * @var boolean $status
     *
     * @ORM\Column(type="boolean", length=1, nullable=false)
     *
     * @Assert\NotBlank
     */
    private $status;


    /**
     * @var User
     *
     * @ORM\OneToMany(targetEntity="app\models\User", cascade={"persist"}, mappedBy="function")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FunctionUser
     */
    public function setName(string $name): FunctionUser
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return FunctionUser
     */
    public function setStatus(bool $status): FunctionUser
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param User addUser
     * @return FunctionUser
     */
    public function addUser(User $user){
        //$this->users[] = $user;
        $this->users->add($user);
        $user->setFunction($this);
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(){
        return $this->users->toArray();
    }
}