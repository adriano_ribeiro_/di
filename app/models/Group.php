<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/11/2018
 * Time: 07:52
 */

namespace app\models;

use app\traits\FieldsGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="app\repositories\group\GroupRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="groups")
 */
class Group extends Model
{
    use FieldsGeneric;

    /**
     * Group constructor.
     */
    public function __construct()
    {
        $this->boards = new ArrayCollection();
        $this->boxes = new ArrayCollection();
    }

    /**
     * @var Board
     *
     * @ORM\ManyToMany(targetEntity="app\models\Board")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo mesa obrigatório")
     */
    private $boards;

    /**
     * @var Box
     *
     * @ORM\ManyToMany(targetEntity="app\models\Box")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo venda obrigatório")
     */
    private $boxes;

    /**
     * @return mixed
     */
    public function getBoards()
    {
        return $this->boards;
    }

    /**
     * @param mixed $board
     * @return Group
     */
    public function addBoard(Board $board)
    {
        $this->boards->add($board);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBoxes()
    {
        return $this->boxes;
    }

    /**
     * @param mixed $box
     * @return Group
     */
    public function addBoxes(Box $box)
    {
        $this->boxes->add($box);
        return $this;
    }
}