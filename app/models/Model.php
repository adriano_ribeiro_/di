<?php
/**
 * Created by PhpStorm.
 * UserModel: adriano
 * Date: 21/07/2018
 * Time: 16:04
 */

namespace app\models;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class Model
{
//    public $entityManager;
//    public $sql;
//    public $session = "user_logged";
//    public $user_id = "user_id";

    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}