<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/11/2018
 * Time: 07:52
 */

namespace app\models;


use app\traits\FieldsGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repositories\product\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="products")
 */
class Product extends Model
{
    use FieldsGeneric;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", unique=true, length=100, nullable=false)
     *
     * @Assert\NotBlank(message="Campo nome obrigatório")
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=200, nullable=false)
     *
     * @Assert\NotBlank(message="Campo descrição obrigatório")
     */
    private $description;

    /**
     * @var float $price
     *
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=false)
     *
     * @Assert\NotBlank(message="Campo preço obrigatório")
     */
    private $price;

    /**
     * @var integer $quantity
     *
     * @ORM\Column(type="integer", nullable=false)
     *
     * @Assert\NotBlank(message="Campo quantidade obrigatório")
     */
    private $quantity;

    /**
     * @var boolean $status
     *
     * @ORM\Column(type="boolean", length=1, nullable=false)
     *
     * @Assert\NotBlank
     */
    private $status;

    /**
     * @var Box
     *
     * @ORM\OneToMany(targetEntity="app\models\Box", mappedBy="product")
     */
    private $boxes;


    public function __construct()
    {
        $this->boxes = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Product
     */
    public function setName(string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Product
     */
    public function setDescription(string $description): Product
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Product
     */
    public function setPrice(float $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Product
     */
    public function setQuantity(int $quantity): Product
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return Product
     */
    public function setStatus(bool $status): Product
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Box
     */
    public function getBoxes(): Box
    {
        return $this->boxes;
    }

    /**
     * @param Box $boxes
     * @return Product
     */
    public function setBoxes(Box $boxes): Product
    {
        $this->boxes = $boxes;
        return $this;
    }
}