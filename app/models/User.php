<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/11/2018
 * Time: 07:51
 */

namespace app\models;


use app\traits\FieldsGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repositories\user\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="users")
 */
class User extends Model
{
    use FieldsGeneric;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     *
     * @Assert\Type(
     *     type="string",
     *     message="Campo nome tem que ser do tipo caracter"
     * )
     *
     * @Assert\NotBlank(message="Campo nome obrigatório")
     */
    private $name;

    /**
     * @var string $email
     *
     * @ORM\Column(type="string", unique=true, length=100, nullable=false)
     *
     * @Assert\NotBlank(message="Campo email obrigatório")
     *
     *@Assert\Email(message="email inválido")
     */
    private $email;

    /**
     * @var string $password
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Campo senha obrigatório")
     *
     * @Assert\Length(min="4", minMessage="Campo senha deve ter no mínimo 4 caracteres")
     *
     */
    private $password;

    /**
     * @var FunctionUser
     *
     * @ORM\ManyToOne(targetEntity="app\models\FunctionUser", cascade={"persist"}, inversedBy="users")
     * @ORM\JoinColumn(name="function_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo função obrigatório")
     */
    private $function;

    /**
     * @var boolean $status
     *
     * @ORM\Column(type="boolean", length=1, nullable=false)
     *
     * @Assert\NotNull
     */
    private $status;

    /**
     * @var Box
     *
     * @ORM\OneToMany(targetEntity="app\models\Box", mappedBy="user")
     */
    private $box;

    public function __construct()
    {
        $this->box = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return FunctionUser
     */
    public function getFunction(): FunctionUser
    {
        return $this->function;
    }

    /**
     * @param FunctionUser $function
     * @return User
     */
    public function setFunction(FunctionUser $function): User
    {
        $this->function = $function;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return User
     */
    public function setStatus(bool $status): User
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Box
     */
    public function getBox(): Box
    {
        return $this->box;
    }

    /**
     * @param Box $box
     * @return User
     */
    public function setBox(Box $box): User
    {
        $this->box = $box;
        return $this;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'email' => $this->getEmail(),
            'function_id' => $this->getFunction()->getName(),
            'status'=> $this->isStatus(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt()
        );
    }
}