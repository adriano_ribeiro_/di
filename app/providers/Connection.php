<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 23/01/2019
 * Time: 09:47
 */

namespace app\providers;


use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;


class Connection
{
    public static function conn(): EntityManager
    {
        $paths = array(base_path('app/models'));
        $isDevMode = true;

        $dbParams = [
            'driver'   => 'pdo_mysql',
            'host'     => '127.0.0.1',
            'port'     => '3306',
            'user'     => 'root',
            'password' => '2710',
            'dbname'   => 'php-di',
            'charset' => 'utf8'
        ];

        try {
            $cache = new ArrayCache();
            $config = new Configuration();
            $config->setMetadataCacheImpl($cache);
            $config = Setup::createConfiguration($isDevMode);
            AnnotationRegistry::registerLoader('class_exists');
            $driverImpl = $config->newDefaultAnnotationDriver($paths);
            $config->setMetadataDriverImpl($driverImpl);
            $config->setQueryCacheImpl($cache);
            $config->setProxyDir(base_path('app/proxies'));
            $config->setProxyNamespace(base_path('app/proxies'));
            $config->setAutoGenerateProxyClasses(true);
            $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, base_path('app/proxies'), $cache, false);
            return EntityManager::create($dbParams, $config);
        } catch (ORMException $e) {
            $e->getMessage();
        }
    }
}