<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 23/01/2019
 * Time: 14:13
 */

namespace app\providers;


use app\utils\TwigFunctions;


abstract class View
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;


    public function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(base_path('resources/views'));
        $twig = new \Twig_Environment($loader);

        $twigFunctions = new \Twig_SimpleFunction(\TwigFunctions::class, function ($method, $params = []) {
            return TwigFunctions::$method($params);
        });
        $twig->addFunction($twigFunctions);

        $this->twig = $twig;
    }


    public function render(string $view, array $data = []): string
    {
        try {
            return $this->twig->render($view, $data);
        } catch (\Twig_Error_Loader $e) {
            $e->getMessage();
        } catch (\Twig_Error_Runtime $e) {
            $e->getMessage();
        } catch (\Twig_Error_Syntax $e) {
            $e->getMessage();
        }
    }
}