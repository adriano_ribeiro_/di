<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 14:26
 */

namespace app\repositories\board;


use app\repositories\AbstractRepository;
use Throwable;


class BoardRepository extends AbstractRepository implements BoardRepositoryInterface
{
    public function fetchName($name)
    {
        try {
            return $this->findOneByName($name);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function all()
    {
        // TODO: Implement all() method.
    }
}