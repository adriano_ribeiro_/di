<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 14:25
 */

namespace app\repositories\board;


interface BoardRepositoryInterface
{
    public function fetchName($name);
    public function all();
}