<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 23/01/2019
 * Time: 11:26
 */

namespace app\repositories\user;


use app\repositories\AbstractRepository;
use Doctrine\ORM\Query\Expr\Join;
use Throwable;

class UserRepository extends AbstractRepository
{
    public function all()
    {
        try {
            //TODO: assim tbm dá certo.
            /** return $this->getEntityManager()->createQueryBuilder()
             * ->select('u.name, u.email, f.name as function_id, u.status, u.createdAt, u.updatedAt')
             * ->from('app\models\User', 'u')
             * ->innerJoin('app\models\FunctionUser', 'f')
             * ->orWhere('u.function = f.id')
             * ->getQuery()
             * ->getResult();  */

            return $this->createQueryBuilder('u')
                ->select('u.id, u.name, u.email, f.name as function_id, u.status, u.createdAt, u.updatedAt')
                ->innerJoin('u.function', 'f', Join::WITH, 'u.function = f.id')
                ->groupBy('u.id')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function statusTrue()
    {
        try {
            return $this->createQueryBuilder("u")
                ->select('u.id, u.name, u.email, f.name as function_id, u.createdAt, u.updatedAt')
                ->innerJoin('u.function', 'f')
                ->where('u.status = :status AND f.name <> :name')
                ->setParameter('status', true)
                ->setParameter('name', 'admin')
                ->groupBy('u.name')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function statusFalse()
    {
        try {
            return $this->createQueryBuilder("u")
                ->select('u.id, u.name, u.email, f.name as function_id, u.status, u.createdAt, u.updatedAt')
                ->innerJoin('u.function', 'f', Join::WITH, 'u.status = :status AND f.name <> :name')
                ->setParameter('status', false)
                ->setParameter('name', 'admin')
                ->groupBy('u.name')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function fetchEmail($email)
    {
        try {
            return $this->findOneByEmail($email);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function fetchName($name)
    {
        try {
            return $this->findOneByName($name);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }
}