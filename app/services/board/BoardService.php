<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 14:20
 */

namespace app\services\board;


use app\exceptions\ValidationException;
use app\factories\BoardFactory;
use app\models\Board;
use app\repositories\board\BoardRepository;
use app\traits\ValidatorTrait;
use app\utils\Json;
use Doctrine\ORM\EntityManager;
use Throwable;


class BoardService extends Json implements BoardServiceInterface
{
    use ValidatorTrait;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var BoardRepository
     */
    private $boardRepository;


    /**
     * BoardService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->boardRepository = $this->em->getRepository(Board::class);
    }

    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            if($this->boardRepository->fetchName($data['name'])) {
                $this->em->rollback();
                return $this->warning($this->codeWarning, $this->messageWarning, $data['name'].' já existe, tente outro.');
            }
            $board = BoardFactory::make($data);
            $this->valid($board);
            $this->boardRepository->save($board);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $board->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}