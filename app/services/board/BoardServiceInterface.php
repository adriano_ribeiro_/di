<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 14:21
 */

namespace app\services\board;


interface BoardServiceInterface
{
    public function save(array $data);
}