<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 28/01/2019
 * Time: 16:04
 */

namespace app\services\user;


use app\exceptions\ValidationException;
use app\factories\UserFactory;
use app\models\FunctionUser;
use app\models\User;;
use app\repositories\user\FunctionUserRepository;
use app\repositories\user\UserRepository;
use app\traits\ValidatorTrait;
use app\utils\Auth;
use app\utils\Json;
use app\utils\Password;
use app\utils\TwigFunctions;
use Aura\Session\Session;
use Doctrine\ORM\EntityManager;
use Throwable;


class UserService extends Json implements UserServiceInterface
{
    use ValidatorTrait;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /** @var FunctionUserRepository */
    private $functionUserRepository;


    /**
     * BoardService constructor.
     * @param EntityManager $em
     * @param Session $session
     */
    public function __construct(EntityManager $em, Session $session)
    {
        $this->em = $em;
        $this->session = $session;
        $this->userRepository = $this->em->getRepository(User::class);
        $this->functionUserRepository = $this->em->getRepository(FunctionUser::class);
    }

    public function home() {
        try {
            $this->session;
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function auth()
    {
        try {
            $this->em->beginTransaction();
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $auth = new Auth($this->session);
            $auth
                ->setEmail($_POST['email'])
                ->setPassword($_POST['password'])
            ;
            $this->valid($auth);

            /** @var User $user */
            $user = $this->userRepository->findOneBy([
                'email' => $_POST['email'],
            ]);
            // se existir o usuário vc pode comparar qualquer outro campo.
            if ($user) {
                if (Password::verify($_POST['password'], $user->getPassword())) {
                    $auth->generateUserSession($user);
                    return redirect('/');
                }
            }
            return $this->info($this->codeInfo, 'Usuário ou Senha incorretos', false);
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function logout () {
        $this->session->getSegment( 'Logged')->clear();
        return redirect('/usuario/login');
        //return $this->success($this->codeSuccess, $this->messageSuccess, false);
    }

    public function saveFunction(array $data)
    {
        // TODO: Implement saveFunction() method.
    }

    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $function = $this->functionUserRepository->getReference($data['function_id']);
            $user = UserFactory::make(array_merge($data, ['function_id' => $function]));
            $this->valid($user);
            $this->userRepository->save($user);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $user->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }

    public function updateStatus($id)
    {
        // TODO: Implement updateStatus() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }

    public function all()
    {
        // TODO: Implement all() method.
    }
}