<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 28/01/2019
 * Time: 15:58
 */

namespace app\services\user;


interface UserServiceInterface
{
    public function auth();

    public function logout();

    public function home();

    public function saveFunction(array $data);

    public function save(array $data);

    public function update($id);

    public function updateStatus($id);

    public function delete();

    public function all();
}