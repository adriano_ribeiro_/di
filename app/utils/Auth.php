<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 28/01/2019
 * Time: 15:42
 */

namespace app\utils;


use app\models\User;
use Aura\Session\Session;
use Symfony\Component\Validator\Constraints as Assert;


class Auth
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var string $email
     *
     * @Assert\NotBlank(message="Campo email obrigatório")
     *
     * @Assert\Email(message="email inválido")
     */
    private $email;


    /**
     * @var string $password
     *
     * @Assert\NotBlank(message="Campo senha obrigatório")
     *
     */
    private $password;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Auth
     */
    public function setEmail(string $email): Auth
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Auth
     */
    public function setPassword(string $password): Auth
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Auth constructor.
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function generateUserSession(User $user)
    {
        $this->session->getSegment('Logged')->set('user', [
            "id" => $user->getId(),
            "name" => $user->getName(),
            "email" => $user->getEmail()
        ]);
    }
}