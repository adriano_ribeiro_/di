<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 23/01/2019
 * Time: 13:45
 */

namespace app\utils;


abstract class Json
{
    protected $codeSuccess = true;
    protected $codeError = false;
    protected $codeWarning = 2;
    protected $codeInfo = 1;
    protected $messageSuccess = 'SUCESSO ';
    protected $messageError = 'ERRO ';
    protected $messageInfo = 'INFORMAÇÃO ';
    protected $messageWarning = 'ADVERTÊNCIA ';
    protected $validation = 'VALIDAÇÃO ';


    public function success ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    public function error ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    public function info ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    public function warning ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }
    
    private function toJson($code, $message, $data)
    {
        header('Content-Type: application/json; charset=UTF-8');

        $json = [
            'code' => $code,
            'message' => $message,
            'data' => $data
        ];

        echo json_encode($json);
        return $json;
    }
}