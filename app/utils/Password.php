<?php
/**
 * Created by PhpStorm.
 * UserModel: adriano
 * Date: 26/07/2018
 * Time: 17:24
 */

namespace app\utils;


class Password
{

    public static function hash($password)
    {
        $options = [
            'cost' => 12
        ];

        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    public static function verify($password, $hash)
    {
        return password_verify($password, $hash);
    }
}