<?php


namespace app\utils;


class Request
{
    public static function request($type)
    {
        if ($_SERVER['REQUEST_METHOD'] != strtoupper($type)) {
            throw new \Exception("A requisição não pode ser do tipo: <b>{$type}</b>");
        }
        return true;
    }
}