<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 23/01/2019
 * Time: 14:26
 */

namespace app\utils;


use Aura\Session\Session;

class TwigFunctions
{
    static $container;

    public static function setContainer(\Psr\Container\ContainerInterface $container)
    {
        self::$container = $container;
    }

    public static function base_url(string $url)
    {
        return base_url($url);
    }

    /**
     *
     * devolve o primeiro segmento da aplicação, útil para navegação
     *
     * @return mixed
     */
    public static function first_uri_segment()
    {
        $page = explode('/', substr($_SERVER['REQUEST_URI'], 1), 2);
        return str_replace("-", " ", $page[0]);
    }

    /**
     * @param $params
     *
     * @return mixed|null|string
     */
    public static function flash($params)
    {
        $session = self::session();
        $flash = $session->getSegment('Logged')->getFlash($params[0]);

        if ($flash) {
            if ($params[0] === 'post') {
                return $flash;
            }

            return sprintf(
                "<div style='width: %s' class='alert alert-%s'>%s</div>",
                '100%',
                $params[1],
                $flash
            );
        }

        return null;
    }

    /**
     * @return mixed
     */
    public static function user_is_logged()
    {
        $session = self::session();
        return $session->getSegment('Logged')->get('user');
    }

    public static function user()
    {
        return self::session()->getSegment('Logged')->get('user');
    }

    public static function session(): Session
    {
        return self::$container->get(Session::class);
    }

    public static function copyright($name)
    {
        return "{$name}, todos los derechos reservados";
    }
}