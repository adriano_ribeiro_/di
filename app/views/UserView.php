<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 09:20
 */

namespace app\views;


use Aura\Session\Session;


class UserView extends ContainerView
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * UserView constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        parent::__construct();
        $this->session = $session;
    }

    public function index()
    {
        echo $this->render('home.twig');
    }

    public function login()
    {
       echo $this->render('user/login.twig');
    }

    public function register()
    {
        echo $this->render('user/register.twig');
    }
}