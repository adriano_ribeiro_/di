<?php


use app\controllers\BoardController;
use app\controllers\UserController;
use app\providers\Connection;
use app\providers\View;
use app\services\board\BoardService;
use app\services\board\BoardServiceInterface;
use app\services\user\UserService;
use app\services\user\UserServiceInterface;
use app\utils\Auth;
use app\utils\TwigFunctions;
use Aura\Session\Session;
use function DI\get;
use Doctrine\ORM\EntityManager;


return [

    EntityManager::class => function () {
        return Connection::conn();
    },

    View::class => \Di\autowire(View::class),
    'SharedContainerTwig' => function (\Psr\Container\ContainerInterface $container) {
        TwigFunctions::setContainer($container);
    },

    \app\views\UserView::class => \Di\autowire(\app\views\UserView::class)->constructor(get(Session::class)),

    UserController::class => \Di\autowire(UserController::class)->constructor(get(UserServiceInterface::class)),
    UserServiceInterface::class => \Di\autowire(UserService::class)->constructor(
        get(EntityManager::class),
        get(Session::class)),

    Session::class => function (): Session {
        return (new \Aura\Session\SessionFactory())->newInstance($_COOKIE);
    },

    Auth::class => \Di\autowire(Auth::class)->constructor(get(Session::class)),

    BoardServiceInterface::class => \Di\autowire(BoardService::class)->constructor(get(EntityManager::class)),
    BoardController::class => \Di\autowire(BoardController::class)->constructor(get(BoardServiceInterface::class)),

];