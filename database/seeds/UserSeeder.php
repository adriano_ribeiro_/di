<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
            $data = [
                'name'          => utf8_encode('Júnior'),
                'email'         => utf8_encode('junior@gmail.com'),
                'password'      => \app\utils\Password::hash('1234'),
                'function_id'   => 3,
                'status'        => true,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ];

        //$this->insert('users', $data);
    }
}
