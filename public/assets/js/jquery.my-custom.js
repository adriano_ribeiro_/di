$.formatStatus = function (status) { // 0 => Inativo, 1 => Ativo
    // CASO SEJA UM INTEIRO
    switch (parseInt(status)) {
        case 0:
            return 'inativo';
        case 1:
            return 'ativo';
    }
    // CASO SEJA BOOLEANO
    switch (status) {
        case false:
            return 'inativo';
        case true:
            return 'ativo';
    }
};

$.fn.serializeObject = function () {
    var formArray = $(this).serializeArray();
    //[{name: 'cpf',value:''}]
    //{cpf: '000',....}
    var obj = {};
    for (var i = 0; i < formArray.length; i++) {
        obj[formArray[i]['name']] = formArray[i]['value'];
    }
    return obj;
};

