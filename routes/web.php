<?php


return \FastRoute\simpleDispatcher(function (\FastRoute\RouteCollector $route) {

    // 'auth' é o indentificador para verificar se o usuário tá logado, vc compara ele la no index.php
    $route->addRoute('GET', '/', ['app\views\UserView', 'index', 'auth']);

    // rotas views user
    $route->addGroup('/usuario', function (\FastRoute\RouteCollector $route) {
        $route->get('/cadastro', ['app\views\UserView', 'register']);
        $route->get('/login', 'app\views\UserView::login');
    });

    // rota action user
    $route->addGroup('/user', function (\FastRoute\RouteCollector $route) {
        $route->post('/auth', 'app\controllers\UserController::auth');
        $route->get('/logout', 'app\controllers\UserController::logout');
        $route->post('/store', 'app\controllers\UserController::store');
    });

    // rota board
    //todo => POSSO USAR ASSIM TBM.
    //$route->addRoute('POST', '/board/store', ['app\controllers\BoardController', 'store']);
    //$route->post('/board/store', ['app\controllers\BoardController', 'store']);
    $route->addGroup('/board', function (\FastRoute\RouteCollector $route) {
        //$route->addRoute('POST', '/store', ['app\controllers\BoardController', 'store']);
        $route->post('/store', 'app\controllers\BoardController::store');
    });
});